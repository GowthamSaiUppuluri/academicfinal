package com.nit.beans;

import java.io.Serializable;

public class FileBean implements Serializable{
	private String fileName;
	private String imagePath;
	

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
