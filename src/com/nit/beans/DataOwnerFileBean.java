package com.nit.beans;

import java.io.Serializable;

public class DataOwnerFileBean extends DataOwnerBean implements Serializable  {
	//properties
	private String file_id;
	private String do_id;
	private String file_data;
	private String file_name;
	//getter & setters
	public String getFile_id() {
		return file_id;
	}
	public void setFile_id(String fileId) {
		file_id = fileId;
	}
	public String getDo_id() {
		return do_id;
	}
	public void setDo_id(String doId) {
		do_id = doId;
	}
	public String getFile_data() {
		return file_data;
	}
	public void setFile_data(String fileData) {
		file_data = fileData;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String fileName) {
		file_name = fileName;
	}
	

}
