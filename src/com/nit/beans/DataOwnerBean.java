package com.nit.beans;

import java.io.Serializable;

public class DataOwnerBean implements Serializable {
	//properties
	private String dataOwner_id;
	private String dataOwner_name;
	private String dataOwner_password;
	private String dataOwner_email;
	private String dataOwner_tpa;
	private String dataOwner_report_time;
	private String dataOwner_location;
	private String Key;
	private String hashcode;
	private String file_id;
	private String c_id;
	private String status;
	public String getHashcode() {
		return hashcode;
	}
	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}
	public String getFile_id() {
		return file_id;
	}
	public void setFile_id(String fileId) {
		file_id = fileId;
	}
	public String getC_id() {
		return c_id;
	}
	public void setC_id(String cId) {
		c_id = cId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getKey() {
		return Key;
	}
	public void setKey(String key) {
		Key = key;
	}
	//getter& setter
	public String getDataOwner_id() {
		return dataOwner_id;
	}
	public void setDataOwner_id(String dataOwnerId) {
		dataOwner_id = dataOwnerId;
	}
	public String getDataOwner_name() {
		return dataOwner_name;
	}
	public void setDataOwner_name(String dataOwnerName) {
		dataOwner_name = dataOwnerName;
	}
	public String getDataOwner_password() {
		return dataOwner_password;
	}
	public void setDataOwner_password(String dataOwnerPassword) {
		dataOwner_password = dataOwnerPassword;
	}
	public String getDataOwner_email() {
		return dataOwner_email;
	}
	public void setDataOwner_email(String dataOwnerEmail) {
		dataOwner_email = dataOwnerEmail;
	}
	public String getDataOwner_tpa() {
		return dataOwner_tpa;
	}
	public void setDataOwner_tpa(String dataOwnerTpa) {
		dataOwner_tpa = dataOwnerTpa;
	}
	public String getDataOwner_report_time() {
		return dataOwner_report_time;
	}
	public void setDataOwner_report_time(String dataOwnerReportTime) {
		dataOwner_report_time = dataOwnerReportTime;
	}
	public String getDataOwner_location() {
		return dataOwner_location;
	}
	public void setDataOwner_location(String dataOwnerLocation) {
		dataOwner_location = dataOwnerLocation;
	}
	

}
