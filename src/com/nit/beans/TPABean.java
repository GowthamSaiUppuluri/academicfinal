package com.nit.beans;

import java.io.Serializable;

public class TPABean implements Serializable {
	//properties of bean class
	private String tpa_id;
	private String tpa_Name;
	private String tpa_email;
	private String tpa_key;
	private String tpa_password;
	//getter setter methods
	
	
	public String getTpa_id() {
		return tpa_id;
	}
	public void setTpa_id(String tpaId) {
		tpa_id = tpaId;
	}
	public String getTpa_Name() {
		return tpa_Name;
	}
	public void setTpa_Name(String tpaName) {
		tpa_Name = tpaName;
	}
	public String getTpa_email() {
		return tpa_email;
	}
	public void setTpa_email(String tpaEmail) {
		tpa_email = tpaEmail;
	}
	public String getTpa_key() {
		return tpa_key;
	}
	public void setTpa_key(String tpaKey) {
		tpa_key = tpaKey;
	}
	public String getTpa_password() {
		return tpa_password;
	}
	public void setTpa_password(String tpaPassword) {
		tpa_password = tpaPassword;
	}
	

}
