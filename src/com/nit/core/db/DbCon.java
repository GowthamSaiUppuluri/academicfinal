package com.nit.core.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.nit.util.DataObject;







/**
 *
 * @author
 */
public class DbCon extends DataObject implements DbInterface{
    
    private Connection con;
    public DbCon() {
    }

    public Connection getConnection() 
    {
        try
        {
            if(con==null)
            {
                try 
                {
                   Properties p = getProperties();
                   Class.forName(p.getProperty("driver"));
                  System.out.println("Driver class-"+p.getProperty("driver"));
                   con = DriverManager.getConnection(p.getProperty("url"),p.getProperty("username"),p.getProperty("password"));
                 
                 
                }
                catch (Exception cnf)
                {
                	cnf.printStackTrace();
                }
            }
        } 
        catch (Exception sqlex) 
        {
        	
        	sqlex.printStackTrace();
        	
        }  
        return con;
    }
    public void close()
    {
        if(con!=null)
        {
            try {
                con.close();
            } catch (SQLException sqlex) {
            	sqlex.printStackTrace();
            }
        }
    }
}
