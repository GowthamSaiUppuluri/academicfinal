package com.nit.util;

import java.sql.Connection;
import java.util.Properties;

public class DataObject{
	
	private static  Connection connection;
	private static Properties props;

	
	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		DataObject.connection = connection;
	}

	
	public Properties getProperties() {
		return props;
	}

	
	public void setProperties(Properties props) {
		DataObject.props = props;
	}

}
