package com.nit.email;

import java.util.Properties;
import java.util.Random;

import javax.mail.*;
import javax.mail.internet.*;

public class EmailUtil {

	// String user="nareshit.pavankumar@gmail.com";//change accordingly
	// String password="214pavan";//change accordingly
	public static String sendingEmail(final String user, final String password,String key,String to_email) {

		// Compose the message
		try {

			//String to = "nareshit.pavankumar@gmail.com";// change accordingly

			// Get the session object
			Properties props = new Properties();
			props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
			props.put("mail.smtp.port", "587"); //TLS Port
			props.put("mail.smtp.auth", "true"); //enable authentication
			props.put("mail.smtp.starttls.enable", "true"); 
			 //create Authenticator object to pass in Session.getInstance argument
			Authenticator auth = new Authenticator() {
				//override the getPasswordAuthentication method
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(user, password);
				}
			};
			Session session = Session.getInstance(props, auth);
			
		/*	Session session = Session.getDefaultInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(user, password);
						}
					});*/

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(user));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to_email));
			message.setSubject("TPA Key");
			
			message.setText(key);

			// send the message
			Transport.send(message);

			System.out.println("message sent successfully...");

		} catch (MessagingException e) {
			e.printStackTrace();
			
		}
		return "message sent successfully...";

	}
}
