package com.nit.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.nit.beans.Constants;
import com.nit.beans.FileBean;
import com.nit.repository.DataOwnerRepositoryImpl;

public class DOUploadFileAction extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public DOUploadFileAction() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String do_id = request.getParameter("do_id");
		
		
		String  filepath="E:\\Block Chain Based Public Integrity Verification Project\\CODE\\Test Files\\";
		 filepath+=request.getParameter("image");
		//String  filepath=request.getParameter("image");
		
		
		String key=request.getParameter("key");
		String target="DO_UploadFile.jsp";
		boolean flag=false;
		//HttpSession session = request.getSession(false);
		String extension = "";
		boolean confirm = false;
		// String uid=(String)session.getAttribute("uid");

	/*	FileBean si = new FileBean();
		DiskFileItemFactory factory = new DiskFileItemFactory();
		ServletFileUpload upload = new ServletFileUpload(factory);
			try {

				List fileItems = upload.parseRequest(request);
				Iterator i = fileItems.iterator();

				int k = 0;
				File f = null;

				while (i.hasNext()) {

					FileItem fi = (FileItem) i.next();

					if (k == 1) {

						String name = fi.getName();
						System.out.println(name);
						extension = name.substring(name.lastIndexOf("."));
						
					
						byte[] bytes = fi.get();
						f = new File(Constants.PATH + name);
						FileOutputStream fos = new FileOutputStream(f);
						fos.write(bytes);
						fos.close();


						confirm = false;

					}

					switch (k) {

					case 0:
						si.setFileName(fi.getString());
						break;

					case 1:

						si.setImagePath(f.toString());
						break;

					case 2:

						si.setFileName(fi.toString());
						break;
					}
					k++;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (extension.equals(".txt")) {
			if (confirm == false) {
				
				
				DataOwnerRepositoryImpl dataOwnerRepositoryImpl=new DataOwnerRepositoryImpl();
				
				try {
					flag=dataOwnerRepositoryImpl.uploadfileIntoDB(do_id, si.getFileName(), key);

					if(flag){
						target="DO_UploadFile.jsp?status=File Uploaded Successfully..";
					}else{
						target="DO_UploadFile.jsp?status=File Not Uploaded";
					}

			*/
		DataOwnerRepositoryImpl dataOwnerRepositoryImpl=new DataOwnerRepositoryImpl();
		File ff=new File(filepath);
		File f=null;
		String name = ff.getName();
		System.out.println(name);
		extension = name.substring(name.lastIndexOf("."));
		
	
		//byte[] bytes = ff.get();
	
		f = new File(Constants.PATH + name);
		File file=new File(filepath);
		
		
		
		FileOutputStream fos = new FileOutputStream(f);

		fos.write((int)file.length());
		fos.close();
		try {
			flag=dataOwnerRepositoryImpl.uploadfileIntoDB(do_id, filepath, key);

			if(flag){
				target="DO_UploadFile.jsp?status=File Uploaded Successfully..";
			}else{
				target="DO_UploadFile.jsp?status=File Not Uploaded";
			}

		
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
			/*}		else {
				target="DO_UploadFile.jsp?status=File Not Uploaded";	}

	} else {
		target="DO_UploadFile.jsp?status=File Not Uploaded";}
		
	*/	response.sendRedirect(target);
		
		

	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
