package com.nit.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.nit.repository.DataOwnerRepositoryImpl;

public class LoginAction extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public LoginAction() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userName = request.getParameter("username");
		String password = request.getParameter("password");
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute("utype", null);
		String target = "signin.jsp?status=Invalid User Name & Password";
		// below are the userNames and passwords please find it
		if (userName.equalsIgnoreCase("admin")
				&& password.equalsIgnoreCase("admin")) {
			httpSession.setAttribute("utype", "admin");
			httpSession.setAttribute("uname", "admin");
			target = "AdminHome.jsp";
		} else if (userName.equalsIgnoreCase("kgc")
				&& password.equalsIgnoreCase("kgc")) {
			httpSession.setAttribute("utype", "kgc");
			httpSession.setAttribute("uname", "kgc");
			target = "KGCHome.jsp";
		} else if (userName.equalsIgnoreCase("cs")
				&& password.equalsIgnoreCase("cs")) {
			httpSession.setAttribute("utype", "cs");
			httpSession.setAttribute("uname", "cs");
			target = "CSHome.jsp";

		} else {
			DataOwnerRepositoryImpl dataOwnerRepositoryImpl = new DataOwnerRepositoryImpl();

			int result = 0;
			try {

				result = dataOwnerRepositoryImpl.authendicateDataOwner(
						userName, password);
				if (result > 0) {
					httpSession.setAttribute("utype", "DataOwner");
					httpSession.setAttribute("uid", String.valueOf(result));
					target = "DataOwnerHome.jsp";

				} else {
					String key = request.getParameter("key");
					result = dataOwnerRepositoryImpl.authendicateTPA(userName,
							password, key);
					if (result > 0) {
						httpSession.setAttribute("utype", "TPA");
						httpSession.setAttribute("uid", String.valueOf(result));
						target = "TPAHome.jsp";

					} else {

						httpSession.setAttribute("utype", null);
						httpSession.setAttribute("utype", null);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		response.sendRedirect(target);
	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
