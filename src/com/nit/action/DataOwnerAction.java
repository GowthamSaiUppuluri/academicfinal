package com.nit.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nit.beans.DataOwnerBean;
import com.nit.beans.TPABean;
import com.nit.email.EmailUtil;
import com.nit.repository.DataOwnerRepositoryImpl;
import com.nit.repository.TPARepositoryImpl;

public class DataOwnerAction extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public DataOwnerAction() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 * 
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int cnt = 0;
		String target = "DataOwnerRegistration.jsp";
		DataOwnerBean dataOwnerBean = new DataOwnerBean();
		dataOwnerBean.setDataOwner_name(request.getParameter("username"));
		dataOwnerBean.setDataOwner_email(request.getParameter("email"));
		dataOwnerBean.setDataOwner_password(request.getParameter("password"));
		dataOwnerBean.setDataOwner_tpa(request.getParameter("tpadetails"));
		dataOwnerBean.setDataOwner_report_time(request.getParameter("report"));
		dataOwnerBean.setDataOwner_location(request.getParameter("location"));
		DataOwnerRepositoryImpl dataOwnerRepositoryImpl = new DataOwnerRepositoryImpl();
		try {

			cnt = dataOwnerRepositoryImpl.addDataownerDetails(dataOwnerBean);

		}

		catch (Exception e) {
			e.printStackTrace();
		}
		if (cnt > 0) {
			target = "DataOwnerRegistration.jsp?status=DataOwner Registration Done";
		} else {
			target = "DataOwnerRegistration.jsp?status=DataOwner Registration Not Done";
		}
		response.sendRedirect(target);

	}

	/**
	 * Initialization of the servlet. <br>
	 * 
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
