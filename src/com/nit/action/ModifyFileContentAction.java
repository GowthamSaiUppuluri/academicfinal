package com.nit.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nit.beans.DataOwnerBean;
import com.nit.beans.DataOwnerFileBean;
import com.nit.repository.DataOwnerRepositoryImpl;

public class ModifyFileContentAction extends HttpServlet {

	

	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}


	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		boolean flag = false;
		String target = "DO_modify__AUDIT_Data.jsp";
		DataOwnerFileBean dataOwnerBean = new DataOwnerFileBean();
		dataOwnerBean.setFile_id(request.getParameter("file_id"));
		dataOwnerBean.setDataOwner_id(request.getParameter("do_id"));
		dataOwnerBean.setHashcode(request.getParameter("hashcode"));
		dataOwnerBean.setFile_data(request.getParameter("filedata"));
		
		DataOwnerRepositoryImpl dataOwnerRepositoryImpl = new DataOwnerRepositoryImpl();
		try {

			flag = dataOwnerRepositoryImpl.modifyFileDataInDB(dataOwnerBean);

		}

		catch (Exception e) {
			e.printStackTrace();
		}
		if (flag) {
			target = "DO_modify__AUDIT_Data.jsp?status=Request Sended";
		} else {
			target = "DO_modify__AUDIT_Data.jsp?status=Failed";
		}
		response.sendRedirect(target);
	}

	

}
