package com.nit.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nit.beans.TPABean;
import com.nit.util.DataObject;

public class TPARepositoryImpl extends DataObject implements TPADataRepository {
	private String TPA_Insert_Query = "INSERT INTO TPADETAILS_TAB VALUES((SELECT NVL(MAX(ID),4444)+1 FROM TPADETAILS_TAB),?,?,?,?)";

	public int addTPADetailsintoDataBase(TPABean tpaBean) throws SQLException,
			ClassNotFoundException {
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement(TPA_Insert_Query);
		ps.setString(1, tpaBean.getTpa_Name());
		ps.setString(2, tpaBean.getTpa_password());
		ps.setString(3, tpaBean.getTpa_email());
		ps.setString(4, tpaBean.getTpa_key());
		int cnt = ps.executeUpdate();
		ps.close();
		return cnt;
	}

	public List<TPABean> getAllTPADetails() throws SQLException,
			ClassNotFoundException {
		Connection con = getConnection();
		List<TPABean> allTpaData = new ArrayList<TPABean>();
		TPABean tpabean = null;
		Statement st = con.createStatement();
		ResultSet rs = st
				.executeQuery("SELECT ID,USERNAME FROM TPADETAILS_TAB");
		while (rs.next()) {
			tpabean = new TPABean();
			tpabean.setTpa_id(rs.getString(1));
			tpabean.setTpa_Name(rs.getString(2));
			allTpaData.add(tpabean);
		}
		
		rs.close();
		st.close();
		return allTpaData;
	}

}
