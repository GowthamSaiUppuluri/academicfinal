package com.nit.repository;

import java.sql.SQLException;
import java.util.List;

import com.nit.beans.TPABean;

public interface TPADataRepository {
	
	// CREATE TABLE TPADETAILS_TAB( ID NUMBER PRIMARY KEY,USERNAME VARCHAR2(40),PASSWORD VARCHAR2(40),EMAIL VARCHAR2(40),KEY VARCHAR2(40));
	//create table dataownerdetails (do_id number,do_name varchar2(40),do_password varchar2(40),do_email varchar2(40),do_tpa varchar2(40),report varchar2(40),location varchar2(40));
	public int addTPADetailsintoDataBase(TPABean tpaBean)throws SQLException,ClassNotFoundException;
	public List<TPABean> getAllTPADetails() throws SQLException,ClassNotFoundException;

}
