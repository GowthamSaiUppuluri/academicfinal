package com.nit.repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.nit.beans.DataOwnerBean;
import com.nit.beans.FileUploadBeanHash;

public interface DataOwnerRepository {
	// create table dataownerdetails (do_id number,do_name
	// varchar2(40),do_password varchar2(40),do_email varchar2(40),do_tpa
	// varchar2(40),report varchar2(40),location varchar2(40), cloudkey number);

	// create table kgc_keys_tab(kgc_id number, dataowner_id varchar2(40));

	// create table do_files_upload (file_id number primary key,do_id
	// varchar2(30),file_data varchar2(4000),file_name varchar2(30));
	public int addDataownerDetails(DataOwnerBean dataOwnerBean)
			throws SQLException, ClassNotFoundException;

	public int authendicateDataOwner(String UserName, String psw)
			throws SQLException, ClassNotFoundException;;

	public boolean checkCloudKeyInfo(String do_id) throws SQLException,
			ClassNotFoundException;

	public boolean checkAndStoreKGCKey(String do_id) throws SQLException,
			ClassNotFoundException;

	public List<DataOwnerBean> getAllStoreKGCKey() throws SQLException,
			ClassNotFoundException;

	public boolean acceptKGCKey(String do_id) throws SQLException,
			ClassNotFoundException;

	public boolean rejectKGCKey(String do_id) throws SQLException,
			ClassNotFoundException;

	public List<DataOwnerBean> getAllStoreKGCKeysList(String uid) throws SQLException,
			ClassNotFoundException;

	public List<DataOwnerBean> getCloudStoreKGCKey() throws SQLException,
			ClassNotFoundException;
	public boolean uploadfileIntoDB(String doId, String fileName, String key) throws SQLException,
	ClassNotFoundException,FileNotFoundException,IOException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException;

	
}
