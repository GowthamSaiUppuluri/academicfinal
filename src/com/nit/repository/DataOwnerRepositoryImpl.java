package com.nit.repository;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import sun.misc.BASE64Encoder;

import com.nit.beans.DataOwnerBean;
import com.nit.beans.DataOwnerFileBean;
import com.nit.util.DataObject;
import com.sun.org.apache.xml.internal.security.utils.Base64;

public class DataOwnerRepositoryImpl extends DataObject implements
		DataOwnerRepository {
	private String DO_Insert_Query = "INSERT INTO DATAOWNERDETAILS VALUES((SELECT NVL(MAX(DO_ID),100)+1 FROM DATAOWNERDETAILS),?,?,?,?,?,?,NULL)";

	public int addDataownerDetails(DataOwnerBean dataOwnerBean)
			throws SQLException, ClassNotFoundException {
		Connection con = getConnection();
		PreparedStatement ps = con.prepareStatement(DO_Insert_Query);
		ps.setString(1, dataOwnerBean.getDataOwner_name());
		ps.setString(2, dataOwnerBean.getDataOwner_password());
		ps.setString(3, dataOwnerBean.getDataOwner_email());
		ps.setString(4, dataOwnerBean.getDataOwner_tpa());
		ps.setString(5, dataOwnerBean.getDataOwner_report_time());
		ps.setString(6, dataOwnerBean.getDataOwner_location());

		int cnt = ps.executeUpdate();
		ps.close();

		return cnt;
	}

	public int authendicateDataOwner(String UserName, String psw)
			throws SQLException, ClassNotFoundException

	{
		int result = 0;

		Connection con = getConnection();
		PreparedStatement ps = con
				.prepareStatement("SELECT DO_ID FROM DATAOWNERDETAILS WHERE DO_NAME=? AND DO_PASSWORD=?");
		ps.setString(1, UserName);
		ps.setString(2, psw);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			result = rs.getInt(1);

		}
		rs.close();
		ps.close();

		return result;
	}

	public int authendicateTPA(String UserName, String psw, String key)
			throws SQLException, ClassNotFoundException

	{
		int result = 0;

		Connection con = getConnection();
		PreparedStatement ps = con
				.prepareStatement("SELECT ID FROM TPADETAILS_TAB WHERE USERNAME=? AND PASSWORD=? AND KEY=?");
		ps.setString(1, UserName);
		ps.setString(2, psw);
		ps.setString(3, key);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			result = rs.getInt(1);

		}
		rs.close();
		ps.close();

		return result;
	}

	public boolean checkCloudKeyInfo(String do_id) throws SQLException,
			ClassNotFoundException {
		boolean flag = false;
		int cnt = 0;
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st
				.executeQuery("SELECT COUNT(*) FROM DATAOWNERDETAILS WHERE CLOUD_KEY IS NULL AND DO_ID="
						+ do_id);
		while (rs.next()) {
			cnt = rs.getInt(1);
		}
		if (cnt > 0) {
			flag = true;
		} else {
			flag = false;
		}

		rs.close();
		st.close();

		System.out.println(flag);
		return flag;
	}

	public boolean checkAndStoreKGCKey(String doId) throws SQLException,
			ClassNotFoundException {
		boolean flag = false;
		int cnt = 0;
		Connection con = getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st
				.executeQuery("SELECT COUNT(*) FROM KGC_KEYS_TAB WHERE  DATAOWNER_ID="
						+ doId);
		PreparedStatement ps = con
				.prepareStatement("INSERT INTO KGC_KEYS_TAB VALUES((SELECT NVL(MAX(KGC_ID),800)+1 FROM KGC_KEYS_TAB),?,0)");
		while (rs.next()) {
			cnt = rs.getInt(1);
		}
		if (cnt > 0) {
			flag = false;
		} else {
			ps.setString(1, doId);
			cnt = ps.executeUpdate();
			if (cnt > 0) {
				flag = true;
			} else {
				flag = false;
			}
		}

		rs.close();
		st.close();

		return flag;
	}

	public List<DataOwnerBean> getAllStoreKGCKey() throws SQLException,
			ClassNotFoundException {
		List<DataOwnerBean> liDataOwnerBeans = new ArrayList<DataOwnerBean>();
		Connection con = getConnection();
		Statement st = con.createStatement();
		DataOwnerBean dataOwnerBean = null;
		ResultSet rs = st
				.executeQuery("select do_id,do_name,do_email from dataownerdetails d,kgc_keys_tab k where d.do_id=k.dataowner_id and d.cloud_key is null and k.CLOUD_APP=1");
		while (rs.next()) {
			dataOwnerBean = new DataOwnerBean();
			dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
			dataOwnerBean.setDataOwner_name(rs.getString(2));
			dataOwnerBean.setDataOwner_email(rs.getString(3));
			liDataOwnerBeans.add(dataOwnerBean);

		}
		rs.close();
		st.close();

		return liDataOwnerBeans;
	}

	public List<DataOwnerBean> getCloudStoreKGCKey() throws SQLException,
			ClassNotFoundException {
		List<DataOwnerBean> liDataOwnerBeans = new ArrayList<DataOwnerBean>();
		Connection con = getConnection();
		Statement st = con.createStatement();
		DataOwnerBean dataOwnerBean = null;
		ResultSet rs = st
				.executeQuery("select do_id,do_name,do_email from dataownerdetails d,kgc_keys_tab k where d.do_id=k.dataowner_id and d.cloud_key is null and k.CLOUD_APP=0");
		while (rs.next()) {
			dataOwnerBean = new DataOwnerBean();
			dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
			dataOwnerBean.setDataOwner_name(rs.getString(2));
			dataOwnerBean.setDataOwner_email(rs.getString(3));
			liDataOwnerBeans.add(dataOwnerBean);

		}
		rs.close();
		st.close();

		return liDataOwnerBeans;
	}

	public List<DataOwnerFileBean> getCloudStoreFileRequest()
			throws SQLException, ClassNotFoundException {
		List<DataOwnerFileBean> liDataOwnerBeans = new ArrayList<DataOwnerFileBean>();
		Connection con = getConnection();
		Statement st = con.createStatement();
		DataOwnerFileBean dataOwnerBean = null;
		ResultSet rs = st
				.executeQuery("SELECT FILE_ID,HASHCODE,C_ID FROM CLOUD_SERVER WHERE STATUS=0");
		while (rs.next()) {
			dataOwnerBean = new DataOwnerFileBean();
			dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
			dataOwnerBean.setHashcode(rs.getString(2));
			dataOwnerBean.setC_id(rs.getString(3));
			liDataOwnerBeans.add(dataOwnerBean);

		}
		rs.close();
		st.close();

		return liDataOwnerBeans;
	}

	public boolean acceptKGCKey(String doId) throws SQLException,
			ClassNotFoundException {
		Connection con = getConnection();
		PreparedStatement ps = con
				.prepareStatement("UPDATE DATAOWNERDETAILS SET CLOUD_KEY=? WHERE DO_ID="
						+ doId);
		ps.setInt(1, new Random().nextInt(8888888));

		int cnt = ps.executeUpdate();
		boolean flag = false;
		if (cnt > 0) {
			flag = true;
		} else
			flag = false;

		ps.close();

		return flag;

	}

	public boolean acceptKGCCloudKey(String doId) throws SQLException,
			ClassNotFoundException {
		Connection con = getConnection();
		PreparedStatement ps = con
				.prepareStatement("UPDATE KGC_KEYS_TAB SET CLOUD_APP=1 WHERE DATAOWNER_ID="+ doId);

		int cnt = ps.executeUpdate();
		boolean flag = false;
		if (cnt > 0) {
			
			flag = true;
		} else
			flag = false;

		ps.close();

		return flag;

	}
	
	
	public boolean modifyFileDataInDB(DataOwnerFileBean dataOwnerFileBean)  {
		boolean flag = false;
		try{
Connection con = getConnection();
PreparedStatement ps = con
		.prepareStatement("UPDATE DO_FILES_UPLOAD SET TPA_AUDIT=0,FILE_DATA=?,ENCRYPT_DATA=? WHERE DO_ID=? AND FILE_ID=?");
ps.setString(1, dataOwnerFileBean.getFile_data());
KeyGenerator keyGen = KeyGenerator.getInstance("AES");
keyGen.init(128);
SecretKey secretkey = keyGen.generateKey();
String stringKey = Base64.encode(secretkey.getEncoded());
System.out.println("scretkey" + stringKey);
Cipher aescipher = Cipher.getInstance("AES");
aescipher.init(Cipher.ENCRYPT_MODE, secretkey);
byte[] byteDataToEncrypt = dataOwnerFileBean.getFile_data().getBytes();
byte[] byteCipherText = aescipher.doFinal(byteDataToEncrypt);
String cipherText = new BASE64Encoder().encode(byteCipherText);
System.out.println(cipherText);
ps.setString(2, cipherText);
ps.setString(3, dataOwnerFileBean.getDataOwner_id());
ps.setString(4, dataOwnerFileBean.getFile_id());
int cnt = ps.executeUpdate();
System.out.println("Counter value is:"+cnt);
PreparedStatement ps1=null;
System.out.println("Counter value vdfsfgdfis:"+cnt);
if (cnt > 0) {
	 ps1 = con
	.prepareStatement("INSERT INTO MODIFY_DATA VALUES((SELECT NVL(MAX(MID),500)+1 FROM MODIFY_DATA),?,?,?,TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'))");
ps1.setString(1, dataOwnerFileBean.getDataOwner_id());
ps1.setString(2, dataOwnerFileBean.getFile_id());
ps1.setString(3, String.valueOf(new Object().hashCode()));
cnt=ps1.executeUpdate();
System.out.println("Counter value is:"+cnt);
if (cnt > 0){
	flag = true;
}else{
	flag = false;
}
} else
	flag = false;

ps.close();}
		catch (Exception e) {
			e.printStackTrace();
		}

return flag;

}
	
	
	

	public boolean acceptTPACloudKey(String cId, String hashcode)
			throws SQLException, ClassNotFoundException {
		Connection con = getConnection();
		Statement st = con.createStatement();
		int cnt = 0;
		boolean flag = false;

		ResultSet rs = st
				.executeQuery("SELECT COUNT(*) FROM DO_FILES_UPLOAD WHERE HASH_CODE ="+hashcode+" AND  TPA_AUDIT=0");
		while (rs.next()) {
			cnt = rs.getInt(1);

		}
		System.out.println(cnt);
		if (cnt > 0) {
			PreparedStatement ps = con
					.prepareStatement("UPDATE CLOUD_SERVER SET STATUS=1 WHERE C_ID="+ cId);

			cnt = ps.executeUpdate();

			if (cnt > 0) {
				flag = true;
			} else
				flag = false;

			ps.close();
		} else {
			
			flag = false;

		}
		return flag;

	}

	public boolean rejectKGCKey(String doId) throws SQLException,
			ClassNotFoundException {
		Connection con = getConnection();
		PreparedStatement ps = con
				.prepareStatement("DELETE FROM KGC_KEYS_TAB  WHERE DATAOWNER_ID="
						+ doId);

		int cnt = ps.executeUpdate();
		boolean flag = false;
		if (cnt > 0) {
			flag = true;
		} else
			flag = false;

		ps.close();

		return flag;

	}

	public List<DataOwnerBean> getAllStoreKGCKeysList(String uid) throws SQLException,
			ClassNotFoundException {
		List<DataOwnerBean> liDataOwnerBeans = new ArrayList<DataOwnerBean>();
		Connection con = getConnection();
		Statement st = con.createStatement();
		DataOwnerBean dataOwnerBean = null;
		ResultSet rs = st
				.executeQuery("select do_id,do_name,CLOUD_KEY from dataownerdetails d,kgc_keys_tab k where d.do_id=k.dataowner_id and d.cloud_key is not null and do_id= "+uid);
		while (rs.next()) {
			dataOwnerBean = new DataOwnerBean();
			dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
			dataOwnerBean.setDataOwner_name(rs.getString(2));
			dataOwnerBean.setKey(String.valueOf(rs.getInt(3)));
			liDataOwnerBeans.add(dataOwnerBean);

		}
		rs.close();
		st.close();
		return liDataOwnerBeans;
	}
	
	public List<DataOwnerBean> getAllStoreKGCKeysList2() throws SQLException,
	ClassNotFoundException {
List<DataOwnerBean> liDataOwnerBeans = new ArrayList<DataOwnerBean>();
Connection con = getConnection();
Statement st = con.createStatement();
DataOwnerBean dataOwnerBean = null;
ResultSet rs = st
		.executeQuery("select do_id,do_name,CLOUD_KEY from dataownerdetails d,kgc_keys_tab k where d.do_id=k.dataowner_id and d.cloud_key is not null");
while (rs.next()) {
	dataOwnerBean = new DataOwnerBean();
	dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
	dataOwnerBean.setDataOwner_name(rs.getString(2));
	dataOwnerBean.setKey(String.valueOf(rs.getInt(3)));
	liDataOwnerBeans.add(dataOwnerBean);

}
rs.close();
st.close();
return liDataOwnerBeans;
}

	public List<DataOwnerBean> getAllStoreKGCKeysList1() throws SQLException,
			ClassNotFoundException {
		List<DataOwnerBean> liDataOwnerBeans = new ArrayList<DataOwnerBean>();
		Connection con = getConnection();
		Statement st = con.createStatement();
		DataOwnerBean dataOwnerBean = null;
		ResultSet rs = st
				.executeQuery("select do_id,do_name,CLOUD_KEY from dataownerdetails d,kgc_keys_tab k where d.do_id=k.dataowner_id and d.cloud_key is not null");
		while (rs.next()) {
			dataOwnerBean = new DataOwnerBean();
			dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
			dataOwnerBean.setDataOwner_name(rs.getString(2));
			dataOwnerBean.setKey(String.valueOf(rs.getInt(3)));
			liDataOwnerBeans.add(dataOwnerBean);

		}
		rs.close();
		st.close();
		return liDataOwnerBeans;
	}
	
	
	
	public List<DataOwnerBean> getAllStoreTPAList() throws SQLException,
	ClassNotFoundException {
List<DataOwnerBean> liDataOwnerBeans = new ArrayList<DataOwnerBean>();
Connection con = getConnection();
Statement st = con.createStatement();
DataOwnerBean dataOwnerBean = null;
ResultSet rs = st
		.executeQuery("select id,USERNAME,EMAIL from TPADETAILS_TAB");
while (rs.next()) {
	dataOwnerBean = new DataOwnerBean();
	dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
	dataOwnerBean.setDataOwner_name(rs.getString(2));
	dataOwnerBean.setKey(rs.getString(3));
	liDataOwnerBeans.add(dataOwnerBean);

}
rs.close();
st.close();
return liDataOwnerBeans;
}

	public DataOwnerBean getAllStoredDOs(String TPA_id) throws SQLException,
			ClassNotFoundException {
		Connection con = getConnection();
		Statement st = con.createStatement();
		DataOwnerBean dataOwnerBean = null;
		ResultSet rs = st
				.executeQuery("select do_id,do_name,CLOUD_KEY from dataownerdetails d,TPADETAILS_TAB t where d.DO_TPA="+TPA_id);
		while (rs.next()) {
			dataOwnerBean = new DataOwnerBean();
			dataOwnerBean.setDataOwner_id(String.valueOf(rs.getInt(1)));
			dataOwnerBean.setDataOwner_name(rs.getString(2));
			dataOwnerBean.setKey(String.valueOf(rs.getInt(3)));

		}
		rs.close();
		st.close();
		return dataOwnerBean;
	}

	@SuppressWarnings("null")
	public boolean uploadfileIntoDB(String doId, String fileName, String key)
			throws SQLException, ClassNotFoundException, IOException,
			IllegalBlockSizeException, BadPaddingException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException {
		boolean flag = false;

		int result = 0;
		if (DataOwnerKeyChecking(key)) {
			Connection con = getConnection();
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO DO_FILES_UPLOAD VALUES((SELECT NVL(MAX(FILE_ID),200)+1 FROM DO_FILES_UPLOAD),?,?,?,?,0,?,TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'))");
			ps.setString(1, doId);
			File file = new File(fileName);
			FileReader fr = new FileReader(file);
			FileInputStream fis = new FileInputStream(file);
			byte[] data = new byte[fis.available()];
			fis.read(data);
			fis.close();
			String filetxt = new String(data);
			Random rd = new Random();

			// InputStream is;
			ps.setCharacterStream(2, fr, (int) file.length());
			ps.setString(3, "fileName" + rd.nextInt(88) + ".txt");
			KeyGenerator keyGen = KeyGenerator.getInstance("AES");
			keyGen.init(128);
			SecretKey secretkey = keyGen.generateKey();
			String stringKey = Base64.encode(secretkey.getEncoded());
			System.out.println("scretkey" + stringKey);
			Cipher aescipher = Cipher.getInstance("AES");
			aescipher.init(Cipher.ENCRYPT_MODE, secretkey);
			byte[] byteDataToEncrypt = filetxt.getBytes();
			byte[] byteCipherText = aescipher.doFinal(byteDataToEncrypt);
			String cipherText = new BASE64Encoder().encode(byteCipherText);
			System.out.println(cipherText);

			ps.setString(5, cipherText);
			ps.setString(4, String.valueOf(new Object().hashCode()));

			result = ps.executeUpdate();
			if (result > 0) {
				flag = true;
			} else {
				flag = false;
			}
			ps.close();
		} else {
			flag = false;
		}
		return flag;
	}

	public boolean DataOwnerKeyChecking(String Key) throws SQLException,
			ClassNotFoundException {
		Connection con = getConnection();
		Statement st = con.createStatement();

		ResultSet rs = st
				.executeQuery("SELECT COUNT(*) FROM DATAOWNERDETAILS WHERE CLOUD_KEY="
						+ Key);
		int cnt = 0;
		boolean flag = false;
		while (rs.next()) {
			cnt = rs.getInt(1);
		}
		if (cnt > 0) {
			flag = true;
		} else
			flag = false;
		st.close();

		return flag;

	}

	public List<DataOwnerFileBean> getFilesForAudit(String tap_id) {
		List<DataOwnerFileBean> liDataOwnerBeans = new ArrayList<DataOwnerFileBean>();
		try {
			Connection con = getConnection();
			Statement st = con.createStatement();
			DataOwnerFileBean dataOwnerBeanFileBean = null;
			ResultSet rs = st
					.executeQuery("select FILE_ID,ENCRYPT_DATA,DO_ID,HASH_CODE from do_files_upload where do_id=(select do_id from dataownerdetails where do_tpa="
							+ tap_id + ")");
			while (rs.next()) {
				dataOwnerBeanFileBean = new DataOwnerFileBean();

				dataOwnerBeanFileBean.setFile_id(rs.getString(1));
				dataOwnerBeanFileBean.setFile_data(rs.getString(2));
				dataOwnerBeanFileBean.setDataOwner_id(String.valueOf(rs
						.getInt(3)));
				dataOwnerBeanFileBean.setFile_name(rs.getString(4));
				liDataOwnerBeans.add(dataOwnerBeanFileBean);

			}
			rs.close();
			st.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liDataOwnerBeans;
	}
	
	
	public List<DataOwnerFileBean> getFilesDataForAudit(String tap_id) {
		List<DataOwnerFileBean> liDataOwnerBeans = new ArrayList<DataOwnerFileBean>();
		try {
			Connection con = getConnection();
			Statement st = con.createStatement();
			DataOwnerFileBean dataOwnerBeanFileBean = null;
			ResultSet rs = st
					.executeQuery("select FILE_ID,FILE_DATA,DO_ID,HASH_CODE,TIME_DATE from do_files_upload where TPA_AUDIT=0 and DO_ID="+ tap_id);
			while (rs.next()) {
				dataOwnerBeanFileBean = new DataOwnerFileBean();

				dataOwnerBeanFileBean.setFile_id(rs.getString(1));
				dataOwnerBeanFileBean.setFile_data(rs.getString(2));
				dataOwnerBeanFileBean.setDataOwner_id(String.valueOf(rs
						.getInt(3)));
				dataOwnerBeanFileBean.setFile_name(rs.getString(4));
				dataOwnerBeanFileBean.setDataOwner_email(rs.getString(5));
				liDataOwnerBeans.add(dataOwnerBeanFileBean);

			}
			rs.close();
			st.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liDataOwnerBeans;
	}
	
	
	public List<DataOwnerFileBean> getFilesDataForFileAudit(String tap_id) {
		List<DataOwnerFileBean> liDataOwnerBeans = new ArrayList<DataOwnerFileBean>();
		try {
			Connection con = getConnection();
			Statement st = con.createStatement();
			DataOwnerFileBean dataOwnerBeanFileBean = null;
			ResultSet rs = st
					.executeQuery("select FILE_ID,FILE_DATA,DO_ID,HASH_CODE,TIME_DATE from do_files_upload where TPA_AUDIT=0 and DO_ID=(SELECT DO_ID FROM DATAOWNERDETAILS WHERE DO_ID=(NVL((SELECT DO_ID FROM DATAOWNERDETAILS WHERE DO_TPA !="+tap_id+"),"+tap_id+")))");
			while (rs.next()) {
				dataOwnerBeanFileBean = new DataOwnerFileBean();

				dataOwnerBeanFileBean.setFile_id(rs.getString(1));
				dataOwnerBeanFileBean.setFile_data(rs.getString(2));
				dataOwnerBeanFileBean.setDataOwner_id(String.valueOf(rs
						.getInt(3)));
				dataOwnerBeanFileBean.setFile_name(rs.getString(4));
				dataOwnerBeanFileBean.setDataOwner_email(rs.getString(5));
				liDataOwnerBeans.add(dataOwnerBeanFileBean);

			}
			rs.close();
			st.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liDataOwnerBeans;
	}
	

	public List<DataOwnerFileBean> getFilesDataForDOAudit(String tap_id) {
		List<DataOwnerFileBean> liDataOwnerBeans = new ArrayList<DataOwnerFileBean>();
		try {
			Connection con = getConnection();
			Statement st = con.createStatement();
			DataOwnerFileBean dataOwnerBeanFileBean = null;
			ResultSet rs = st
					.executeQuery("select FILE_ID,FILE_DATA,DO_ID,HASH_CODE from do_files_upload where TPA_AUDIT=0 and do_id=(select do_id from dataownerdetails where do_id="
							+ tap_id + ")");
			while (rs.next()) {
				dataOwnerBeanFileBean = new DataOwnerFileBean();

				dataOwnerBeanFileBean.setFile_id(rs.getString(1));
				dataOwnerBeanFileBean.setFile_data(rs.getString(2));
				dataOwnerBeanFileBean.setDataOwner_id(String.valueOf(rs
						.getInt(3)));
				dataOwnerBeanFileBean.setFile_name(rs.getString(4));
				liDataOwnerBeans.add(dataOwnerBeanFileBean);

			}
			rs.close();
			st.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return liDataOwnerBeans;
	}

	public int addCloudRequestDetails(DataOwnerBean dataOwnerBean)
			throws SQLException, ClassNotFoundException {
		Connection con = getConnection();
		Statement st = con.createStatement();
		int cnt = 0;

		ResultSet rs = st
				.executeQuery("SELECT COUNT(*) FROM CLOUD_SERVER WHERE HASHCODE ="
						+ dataOwnerBean.getDataOwner_password());
		while (rs.next()) {
			cnt = rs.getInt(1);

		}
		System.out.println(cnt);
		PreparedStatement ps = null;
		if (cnt > 2) {

		} else {
			ps = con
					.prepareStatement("INSERT INTO CLOUD_SERVER VALUES((SELECT NVL(MAX(C_ID),3333)+1 FROM CLOUD_SERVER),?,?,TO_CHAR(SYSDATE, 'MM-DD-YYYY HH24:MI:SS'),0)");
			ps.setString(1, dataOwnerBean.getDataOwner_name());
			ps.setString(2, dataOwnerBean.getDataOwner_password());

			cnt = ps.executeUpdate();
			ps.close();
		}

		return cnt;
	}

}
