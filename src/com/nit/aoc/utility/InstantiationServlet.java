package com.nit.aoc.utility;


import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;

import com.nit.core.db.DbCon;
import com.nit.util.DataObject;






@SuppressWarnings("serial")
public class InstantiationServlet extends HttpServlet 
{	
    public void init(ServletConfig sc)
    {
    	ServletContext ctx = sc.getServletContext();
    	DataObject dobject = new DataObject();
    	InputStream fis = ctx.getResourceAsStream(sc.getInitParameter("config"));
    	Properties props = new Properties();
   	 	try 
   	 	{System.out.println("Instantionstation servlet Called");
			props.load(fis);
		} 
   	 	catch (IOException ioe) {
   	 		ioe.printStackTrace();
			
		}
    	dobject.setProperties(props);
    	
    	
    	DbCon dbcon = new DbCon();
    	Connection connection = dbcon.getConnection();
    	dobject.setConnection(connection);
    	
    	
    }
}
