
<%@page import="com.nit.repository.TPARepositoryImpl"%>
<%@page import="java.util.List"%>
<%@page import="com.nit.beans.TPABean"%><!DOCTYPE HTML>
<html>

<head>
  <title>Block Chain</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />

</head>
<script> 
function TPAValidationCode()                                    
{ 
    var name = document.forms["tpaform"]["username"];               
    var email = document.forms["tpaform"]["email"];    
    var password = document.forms["tpaform"]["password"];  
    var tpadetails=document.forms["tpaform"]["tpadetails"];
    var report=document.forms["tpaform"]["report"];
    var location=document.forms["tpaform"]["location"];
    
    var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (name.value == "")                                  
    { 
        window.alert("Please enter User Name."); 
        name.focus(); 
        return false; 
    }else  if (password.value == "")                                  
    { 
        window.alert("Please enter Password."); 
        password.focus(); 
        return false; 
    }else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value))                                   
    { 
        window.alert("Please enter a valid e-mail address."); 
        email.focus(); 
        return false; 
    }
    if(tpadetails.value==""){
            window.alert("Please Choose."); 
        tpadetails.focus(); 
        return false; 
    }
     if(report.value==""){
            window.alert("Please Choose report."); 
        report.focus(); 
        return false; 
    }
     if(location.value==""){
            window.alert("Please Enter location."); 
        location.focus(); 
        return false; 
    }
    else{
       return true; }
}</script> 
  
<body>
  <div id="main">
   <jsp:include page="header.jsp"></jsp:include>
    <div id="content_header"></div>
    <div id="site_content"><br><div id="sidebar_container">
      
       </div>
        <div id="content">
        <!-- insert the page content here -->
        <h1 style="text-align: center">Enter DataOwner  Details</h1>
        <p>
        <%if(request.getParameter("status")!=null){
        out.println(request.getParameter("status"));
        }
         %>
        </p>       
          <div class="form_settings">
          <form  name="tpaform" onsubmit="return TPAValidationCode()" action="./DataOwnerAction" method="post" >
            <p><span>UserName</span><input class="contact" type="text" name="username"  required/></p>
            <p><span>Password</span><input class="contact" type="password" name="password"  required/></p>
              <p><span>Email</span><input class="contact" type="email" name="email"  required/></p>
              <p><span>Select TPA</span><select class="contact" name="tpadetails" required >
              <option value="">--Select Any One--</option>
              <%
              TPARepositoryImpl tpaRepositoryImpl=new TPARepositoryImpl();
              List<TPABean> tpalist=tpaRepositoryImpl.getAllTPADetails();
              for(TPABean tpaBean:tpalist){
              System.out.println(tpaBean.getTpa_id());
              %>
              <option value=<%=tpaBean.getTpa_id() %>><%=tpaBean.getTpa_Name() %>
              <%}
              
               %></option>
              </select>
              
              
              </p>
              <p><span>Report Time</span><select class="contact" name="report" required>
              <option value="">--Select Any One--</option>
              
              <option value="1">1-Day</option>
              <option value="1">1-Month</option>
              <option value="1">1-Year</option>
              
              </select>
              
              
              </p>
              <p><span>Location</span><input class="contact" type="text" name="location"  required/></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="submit" /></p>
        </form>
          </div>       
        <p><br /><br /></p>
      </div>
    </div>
  <jsp:include page="footer.jsp"></jsp:include>
  </div>
</body>
</html>
