<!DOCTYPE HTML>
<html>

<head>
  <title>Block Chain</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>
  <div id="main">
   <jsp:include page="header.jsp"></jsp:include>
    <div id="content_header"></div>
    <div id="site_content"><br><div id="sidebar_container">
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>Blockchain-Based Public Integrity Verification for
Cloud Storage against Procrastinating Auditors</h1>
         <ul>
          <li>Public verification techniques can enable a user to employ a third-party
auditor to verify the data integrity on behalf of her/him, whereas existing public verification schemes are vulnerable to procrastinating
auditors who may not perform verifications on time. </li>
          <li>Furthermore, most of public verification schemes are constructed on the public key
infrastructure (PKI), and thereby suffer from certificate management problem. In this paper, we propose the first certificateless public
verification scheme against procrastinating auditors (CPVPA) by using blockchain technology.</li>
          <li> The key idea is to require auditors to
record each verification result into a blockchain as a transaction. </li>
<li>Since transactions on the blockchain are time-sensitive, the
verification can be time-stamped after the corresponding transaction is recorded into the blockchain, which enables users to check
whether auditors perform the verifications at the prescribed time.</li>
        </ul>
      </div>
    </div>
  <jsp:include page="footer.jsp"></jsp:include>
  </div>
</body>
</html>
