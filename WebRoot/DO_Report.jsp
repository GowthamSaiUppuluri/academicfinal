<!DOCTYPE HTML>
<html>

<head>
  <title>Block Chain</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
<script type="text/javascript" src="style/canvas.js"></script></head>

<body>
  <div id="main">
   <jsp:include page="header.jsp"></jsp:include>
    <div id="content_header"></div>
    <div id="site_content"><br><div id="sidebar_container">
      
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
           <jsp:include page="do_menus.jsp"/>
            </div>
          <div class="sidebar_base"></div>
        </div>
       
      </div>
      <div id="content">
        <!-- insert the page content here -->
      
       <div id="chartContainer" style="height: 300px; width: 100%;"></div>
      </div>
    </div>
  <jsp:include page="footer.jsp"></jsp:include>
  </div>
  
</body>
<script>
window.onload = function() {

var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	title: {
		text: "File Uploaded Performance"
	},
	data: [{
		type: "pie",
		startAngle: 240,
		yValueFormatString: "##0.00\"%\"",
		indexLabel: "{label} {y}",
		dataPoints: [
			{y: 79.45, label: "Free Space"},
			{y: 7.31, label: "FileData"},
			/*{y: 7.06, label: "Baidu"},
			{y: 4.91, label: "Yahoo"},
			{y: 1.26, label: "Others"}*/
		]
	}]
});
chart.render();

}
</script>
</html>
