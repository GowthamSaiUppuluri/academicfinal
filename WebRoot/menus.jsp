 <%
 String utype=(String)session.getAttribute("utype");
 if(utype==null){
 %><div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="index.jsp">Home</a></li>
          <li><a href="signin.jsp">Sign In</a></li>
          <li><a href="DataOwnerRegistration.jsp">DORegistration</a></li>
          <li><a href="TPASignin.jsp">TPA Sign In</a></li>
         <li><a href="contact.jsp">Contact Us</a></li>
        </ul>
      </div><%
 }
 else if(utype.equalsIgnoreCase("admin"))
 { %><div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li class="selected"><a href="AdminHome.jsp">Home</a></li>
          <li><a href="AddTPA.jsp">Add TPA</a></li>
          <li><a href="AdminDO_list.jsp">View TPA's</a></li>
          <li><a href="./LogoutAction">Sign Out</a></li>
        </ul>
      </div><%
 }
 else if(utype.equalsIgnoreCase("kgc"))
 {
  %><div id="menubar">
        <ul id="menu">
          <li class="selected"><a href="KGCHome.jsp.jsp">Home</a></li>
          <li><a href="DO_TO_KGCCRequest.jsp">DataOwner  Req..</a></li>
          <li><a href="DO_TO_KGCCResponse.jsp">DataOwners List</a></li>
          <li><a href="./LogoutAction">Sign Out</a></li>
        </ul>
      </div><%
 }else if(utype.equalsIgnoreCase("cs")){
  %><div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li class="selected"><a href="CSHome.jsp">Home</a></li>
                    <li><a href="DO_TO_CloudRequest.jsp">DataOwner Request</a></li>
                    <li><a href="TPA_TO_CloudRequest.jsp">TPA File Request</a></li>
                 
                    <li><a href="./LogoutAction">Sign Out</a></li>
        </ul>
      </div><%
 }else if(utype.equalsIgnoreCase("DataOwner")){
  %><div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li class="selected"><a href="DataOwnerHome.jsp">Home</a></li>
          
          <li><a href="./LogoutAction">Sign Out</a></li>
        </ul>
      </div><%
 }else if(utype.equalsIgnoreCase("TPA")){
  %><div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li class="selected"><a href="TPAHome.jsp">Home</a></li>
         <li><a href="./LogoutAction">Sign Out</a></li>
        </ul>
      </div><%
 }else{
 } %>
 
 
 
 