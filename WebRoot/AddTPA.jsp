<!DOCTYPE HTML>
<html>

<head>
  <title>Block Chain</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />

</head>
<script> 
function TPAValidationCode()                                    
{ 
    var name = document.forms["tpaform"]["username"];               
    var email = document.forms["tpaform"]["email"];    
    var password = document.forms["tpaform"]["password"];  
    var reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (name.value == "")                                  
    { 
        window.alert("Please enter User Name."); 
        name.focus(); 
        return false; 
    }else  if (password.value == "")                                  
    { 
        window.alert("Please enter Password."); 
        password.focus(); 
        return false; 
    }else if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.value))                                   
    { 
        window.alert("Please enter a valid e-mail address."); 
        email.focus(); 
        return false; 
    }else{
       return true; }
}</script> 
  
<body>
  <div id="main">
   <jsp:include page="header.jsp"></jsp:include>
    <div id="content_header"></div>
    <div id="site_content"><br><div id="sidebar_container">
      
        <!--<div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="#">link 1</a></li>
              <li><a href="#">link 2</a></li>
              <li><a href="#">link 3</a></li>
              <li><a href="#">link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
       
      --></div>
        <div id="content">
        <!-- insert the page content here -->
        <h1 style="text-align: center">Add TPA Details</h1>
        <p>
        <%if(request.getParameter("status")!=null){
        out.println(request.getParameter("status"));
        }
         %>
        </p>       
          <div class="form_settings">
          <form  name="tpaform" onsubmit="return TPAValidationCode()" action="./AddTPADetails" method="post" >
            <p><span>UserName</span><input class="contact" type="text" name="username"  required/></p>
            <p><span>Password</span><input class="contact" type="password" name="password"  required/></p>
              <p><span>Email</span><input class="contact" type="email" name="email"  required/></p>
            <p style="padding-top: 15px"><span>&nbsp;</span><input class="submit" type="submit" name="contact_submitted" value="submit" /></p>
        </form>
          </div>       
        <p><br /><br /></p>
      </div>
    </div>
  <jsp:include page="footer.jsp"></jsp:include>
  </div>
</body>
</html>
