
<%@page import="com.nit.repository.DataOwnerRepositoryImpl"%>
<%@page import="java.util.List"%>
<%@page import="com.nit.beans.DataOwnerBean"%>
<%@page import="com.nit.beans.DataOwnerFileBean"%>
<%@page import="javax.mail.Session"%><!DOCTYPE HTML>
<html>

<head>
  <title>Block Chain</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>
  <div id="main">
   <jsp:include page="header.jsp"></jsp:include>
    <div id="content_header"></div>
  <div id="site_content"><br><div id="sidebar_container">
      
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
           <jsp:include page="TPA_menus.jsp"/>
            </div>
          <div class="sidebar_base"></div>
        </div>
       
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1></h1><table>
      
       <%
       DataOwnerRepositoryImpl dataOwnerRepositoryImpl=new DataOwnerRepositoryImpl();
      List<DataOwnerFileBean> listDataOwnerBeans=dataOwnerRepositoryImpl.getFilesForAudit((String)session.getAttribute("uid"));
      session.setAttribute("filesdata",listDataOwnerBeans);
         if(listDataOwnerBeans.size()>0){
      %>
      <tr>
     <td>HashCode</td> <td>DataOwner Id</td><td>FileId</td><td>FileData</td>  <td>FileName</td>
      </tr><tr>
      <%for(DataOwnerFileBean bean:listDataOwnerBeans){
      
      %>
      <td> <a href="./TPACLoudFileRequestAction?file_id=<%=bean.getFile_id() %>&do_id=<%=bean.getDataOwner_id() %>&hashcode=<%=bean.getFile_name() %>"><%=bean.getFile_name() %></a></td>
      
      <td><%=bean.getDataOwner_id() %></td>
      <td><%=bean.getFile_id() %></td>
      <td><%=bean.getFile_data() %></td>
           </tr>
      
     <% } %>

     <%
      }else{
      
        %>
       <h1>No Request Found</h1>
      <%}%>
      
      </table>
      
      
      
      
      </div>
    </div>
  <jsp:include page="footer.jsp"></jsp:include>
  </div>
</body>
</html>
